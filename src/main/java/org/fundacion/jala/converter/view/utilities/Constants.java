package org.fundacion.jala.converter.view.utilities;

public class Constants {
    public static final String HTTP_URL_CONVERT_AUDIO = "http://localhost:8080/api/convertAudio";
    public static final String HTTP_URL_CONVERT_VIDEO = "http://localhost:8080/api/convertVideo";
    public static final String HTTP_URL_EXTRACT_TEXT = "http://localhost:8080/api/extractText";
    public static final String HTTP_URL_EXTRACT_METADATA = "http://localhost:8080/api/extractMetadata";
    public static final String HTTP_URL_CONVERT_IMAGE = "http://localhost:8080/api/convertImage";
    public static final String DIR_DOWNLOAD = "/Downloads/";
    public static final String USER_ID = "1";
    public static final String AUDIO_FORMAT_SUPPORT = "avi,mp4,mpeg,mov,wmv,webm,mp3,wav,mkv,flv";
    public static final String VIDEO_FORMAT_SUPPORT = "avi,mp4,mpeg,mov,wmv,webm,mkv,flv";
    public static final String TEXT_FORMAT_SUPPORT = "png,jpg,jpeg,tiff,bmp";
    public static final String IMAGE_FORMAT_SUPPORT = "png,jpg,jpeg,tiff,bmp,gif";
    public static final String USR_PATH = System.getProperty("user.dir");
}
